class ChangeColumnToArticle < ActiveRecord::Migration
  def change
    remove_column :articles, :source_id, :integer
    add_column :articles, :root, :string
    add_column :articles, :intro, :string
  end
end
