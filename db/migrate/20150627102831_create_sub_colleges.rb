class CreateSubColleges < ActiveRecord::Migration
  def change
    create_table :sub_colleges do |t|
      t.string :name

      t.timestamps
    end
  end
end
