class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.belongs_to :user
      t.text :content
      t.string :title
      t.string :label
      t.integer :read_num
      t.integer :owner_id

      t.timestamps
    end
  end
end
