class CreateAssociations < ActiveRecord::Migration
  def change
    create_table :associations do |t|
      t.integer :A_id
      t.string :A_type
      t.integer :B_id
      t.string :B_type
      t.string :AB_association

      t.timestamps
    end
  end
end
