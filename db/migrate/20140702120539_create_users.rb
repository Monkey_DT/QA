class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :phone, unique: true
      t.string :name
      t.string :username
      t.string :real_name
      t.string :stu_num
      t.string :user_sign
      t.boolean :sex
      t.date :birth
      t.text :hobby
      t.string :head_image
      t.string :school_image
      t.integer :clazz
      t.string :default_org
      t.string :pinyin
      t.string :remark

      t.belongs_to :organization, index: true
      t.belongs_to :parent, index: true
      t.belongs_to :society, index: true
      t.belongs_to :fellow, index: true
      t.belongs_to :major, index: true
      t.belongs_to :sub_college, index: true

      t.timestamps
    end
  end
end
