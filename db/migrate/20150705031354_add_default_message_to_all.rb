class AddDefaultMessageToAll < ActiveRecord::Migration
  def change
    change_table(:users) do |t|
      t.remove :sex
      t.remove :rank
      t.remove :is_approve
      t.remove :fellow_id
      t.remove :major_id
      t.integer :sex ,null: false, default: 1
      t.integer :rank ,null: false, default: 0
      t.integer :is_approve ,null: false, default: 0
      t.integer :fellow_id ,null: false, default: 0
      t.integer :major_id ,null: false, default: 0
    end

    change_table(:majors) do |t|
      t.remove :no
      t.remove :name
    t.integer :no ,null: false, default: 0
    t.string :name ,null: false, default: ""
    end

    change_table(:sub_colleges) do |t|
      t.remove :name
      t.string :name ,null: false, default: 0
    end

    change_table(:questions) do |t|
      t.remove :read_num
      t.remove :owner_id
      t.integer :read_num ,null: false, default: 0
      t.integer :owner_id ,null: false, default: 0
    end

    change_table(:answers) do |t|
      t.remove :praise_num
      t.integer :praise_num ,null: false, default: 0
    end

    change_table(:article_types) do |t|
      t.remove :prosition
      t.integer :prosition,null: false, default: 0
    end

    change_table(:associations) do |t|
      t.remove :A_type
      t.remove :B_type
      t.remove :AB_association
      t.string :A_type ,null: false, default: ""
      t.string :B_type ,null: false, default: ""
      t.string :AB_association ,null: false, default: ""
    end

    change_table(:societies) do |t|
      t.remove :master_id
      t.integer :master_id ,null: false, default: 0
    end

    end
end
