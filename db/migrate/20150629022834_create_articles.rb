class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.text :content
      t.belongs_to :article_type
      t.string :author
      t.string :link
      t.belongs_to :source

      t.timestamps
    end
  end
end
