class CreateNotices < ActiveRecord::Migration
  def change
    create_table :notices do |t|
      t.text :content
      t.string :title
      t.integer :type
      t.integer :extra_id
      t.integer :extra_type

      t.timestamps
    end
  end
end
