class CreateSocieties < ActiveRecord::Migration
  def change
    create_table :societies do |t|
      t.string :cover
      t.string :name
      t.integer :master_id
      t.string :telephone
      t.string :introduction
      t.string :link

      t.timestamps
    end
  end
end
