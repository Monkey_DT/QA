class CreateMajors < ActiveRecord::Migration
  def change
    create_table :majors do |t|
      t.integer :no
      t.string :name
      t.references :sub_college, index: true

      t.timestamps
    end
  end
end
