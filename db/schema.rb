# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150705041351) do

  create_table "answers", force: true do |t|
    t.text     "content"
    t.integer  "user_id"
    t.string   "user_role"
    t.integer  "question_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "praise_num",          default: 0, null: false
  end

  add_index "answers", ["question_id"], name: "index_answers_on_question_id", using: :btree
  add_index "answers", ["user_id"], name: "index_answers_on_user_id", using: :btree

  create_table "article_types", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "prosition",  default: 0, null: false
  end

  create_table "articles", force: true do |t|
    t.text     "content"
    t.integer  "article_type_id"
    t.string   "author"
    t.string   "link"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "root"
    t.string   "intro"
    t.string   "title"
  end

  create_table "associations", force: true do |t|
    t.integer  "A_id"
    t.integer  "B_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "extra_message"
    t.string   "A_type",         default: "", null: false
    t.string   "B_type",         default: "", null: false
    t.string   "AB_association", default: "", null: false
  end

  create_table "attachments", force: true do |t|
    t.string   "name"
    t.integer  "author_id"
    t.integer  "position",            default: 100
    t.integer  "data_type",           default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  add_index "attachments", ["author_id"], name: "index_attachments_on_author_id", using: :btree

  create_table "code_tables", force: true do |t|
    t.string   "name"
    t.integer  "parent_id"
    t.integer  "position",      default: 100
    t.string   "remark"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.string   "default_value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "code_tables", ["parent_id"], name: "index_code_tables_on_parent_id", using: :btree

  create_table "fellows", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "majors", force: true do |t|
    t.integer  "sub_college_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "no",             default: 0,  null: false
    t.string   "name",           default: "", null: false
  end

  add_index "majors", ["sub_college_id"], name: "index_majors_on_sub_college_id", using: :btree

  create_table "notices", force: true do |t|
    t.text     "content"
    t.string   "title"
    t.integer  "notice_type"
    t.integer  "extra_id"
    t.integer  "extra_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "organizations", force: true do |t|
    t.string   "name"
    t.integer  "parent_id"
    t.integer  "position",   default: 100
    t.string   "remark"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "organizations", ["parent_id"], name: "index_organizations_on_parent_id", using: :btree

  create_table "permissions", force: true do |t|
    t.string   "action"
    t.string   "subject"
    t.string   "description"
    t.string   "code"
    t.integer  "group_id"
    t.string   "fetching"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "permissions", ["group_id"], name: "index_permissions_on_group_id", using: :btree

  create_table "questions", force: true do |t|
    t.integer  "user_id"
    t.text     "content"
    t.string   "title"
    t.string   "label"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "read_num",            default: 0, null: false
    t.integer  "owner_id",            default: 0, null: false
  end

  create_table "role_permissions", force: true do |t|
    t.integer  "role_id"
    t.integer  "permission_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "role_permissions", ["permission_id"], name: "index_role_permissions_on_permission_id", using: :btree
  add_index "role_permissions", ["role_id"], name: "index_role_permissions_on_role_id", using: :btree

  create_table "roles", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "position",    default: 100
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "societies", force: true do |t|
    t.string   "cover"
    t.string   "name"
    t.string   "telephone"
    t.string   "introduction"
    t.string   "link"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "master_id",    default: 0, null: false
  end

  create_table "sources", force: true do |t|
    t.string   "link"
    t.string   "intro"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sub_colleges", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",       default: "0", null: false
  end

  create_table "user_roles", force: true do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_roles", ["role_id"], name: "index_user_roles_on_role_id", using: :btree
  add_index "user_roles", ["user_id"], name: "index_user_roles_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "phone"
    t.string   "name"
    t.string   "username"
    t.string   "real_name"
    t.string   "stu_num"
    t.string   "user_sign"
    t.date     "birth"
    t.text     "hobby"
    t.string   "head_image"
    t.string   "school_image"
    t.integer  "clazz"
    t.string   "default_org"
    t.string   "pinyin"
    t.string   "remark"
    t.integer  "organization_id"
    t.integer  "parent_id"
    t.integer  "society_id"
    t.integer  "fellow_id"
    t.integer  "major_id"
    t.integer  "sub_college_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.integer  "sex",                    default: 1,  null: false
    t.integer  "rank",                   default: 0,  null: false
    t.integer  "is_approve",             default: 0,  null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["fellow_id"], name: "index_users_on_fellow_id", using: :btree
  add_index "users", ["major_id"], name: "index_users_on_major_id", using: :btree
  add_index "users", ["organization_id"], name: "index_users_on_organization_id", using: :btree
  add_index "users", ["parent_id"], name: "index_users_on_parent_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["society_id"], name: "index_users_on_society_id", using: :btree
  add_index "users", ["sub_college_id"], name: "index_users_on_sub_college_id", using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

end
