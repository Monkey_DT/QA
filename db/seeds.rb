# coding: UTF-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

org = Organization.create!({name: '基本组织', position: 0})
User.create({name: '管理员', phone: '13988887777', email: 'admin@qq.com', password: '12345678', password_confirmation: '12345678', organization:org})