class Front::AssociationController < ActionController::Base
  def judge_praise
    user_id = params[:user_id]
    answer_id = params[:answer_id]

    @association = Association.where('A_id=? AND A_type=? AND B_id=? AND B_type=?',
                                     user_id, 'users', answer_id, 'answers').first

    if @association
      if @association.extra_message = '1'
        render json: {message: 'no'}
      else
        @association.update_attribute(extra_message,1)
        render json: {message: 'yes'}
      end
    else
     render json: {message:'yes'}
    end
  end

  def fellow_list
    name = params[:name]
    @fellow = Fellow.find_by(name: name)
    if @fellow
      render json: @fellow.users
    else
      render json: {message:'fellow name is not exsit'} , statuts: :not_found
    end

  end
end
