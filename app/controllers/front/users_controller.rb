class Front::UsersController < Front::BaseController
  # 注册用户
  def sign_up
    fellow_name = params[:fellow_name]
    @fellow = Fellow.find_by(name: fellow_name)
    if @fellow
      @user = User.new username: params[:username], email: params[:email],
                       password: params[:password], fellow_id: @fellow.id
      #, password_confimation: params[:password]
      if @user.save
        render json: @user, status: :created
        #   .as_json.except[:created_at , :updated_at],
      else
        render json: @user.errors, status: :bad_request
      end
    else
      render json: {message:'省份不存在'},status: :not_found
    end
  end

  def sign_in_for_web
    @user = User.find_for_database_authentication(login: params[:login].downcase)
    return render json: {message: '该用户不存在'}, status: :not_found unless @user
    if @user.valid_password?(params[:password])
      sign_in @user
      render json: @user
    else
      render json: {message: '密码错误！'}, status: :bad_request
    end
  end

  def sign_out_for_web
    sign_out current_user
    render json: {messages: '退出登陆成功'}, status: :ok
  end

  # 登陆
  def validate!
    @user = User.find_for_database_authentication(login: params[:login].downcase)
    return render json: {message: '没用该用户'}, status: :bad_request unless @user
    if @user.valid_password?(params[:password])
      @user.ensure_authentication_token
      @user.save
    else
      return render json: {message: '密码错误'}, status: :bad_request
    end
    render json: @user
  end

  # 获取当前用户对象
  def current
    render json: current_user.to_all_json
  end

  # 获取个人信息
  def show
    user = User.find_by(id: params[:id])
    if user
      render json: user.to_all_json
    else
      render json: {message: 'id not exsit'}
    end
  end

  # 编辑个人信息
  def update_only_four_info
    username = params[:username]
    hobby = params[:hobby]
    user_sign = params[:user_sign]
    sex = params[:sex]
    @user = User.all.find_by(id: params[:user_id])
    if @user
      if @user.update_attributes(username: username, hobby: hobby, user_sign: user_sign, sex: sex)
        render json: @user.to_all_json, status: :ok
      else
        render json: @user.errors, status: :bad_request
      end
    else
      render json: {message: 'id is not exist'}, status: :not_found
    end
  end

  # 更改个人信息
  def update
    u_params = user_params
    if u_params[:password].blank?
      u_params.delete(:password)
    else
      u_params[:password_confirmation] = u_params[:password]
    end
    @user = User.all.find_by(id: params[:user_id])
    if @user
      if @user.update_attributes(u_params)
        render json: @user.to_all_json, status: :created
      else
        render json: @user.errors, status: :bad_request
      end
    else
      render json: {message: 'id is not exist'}, status: :not_found
    end

  end

  # 获取排名
  def get_rank
    @user = User.find_by(id: params[:user_id])
    if @user
      render json: {rank: @user.rank}, status: :ok
    else
      render json: {message: 'id is not exsit'}, status: :not_found
    end
  end

  # 获取总排名
  def get_total_rank
    page = params[:page] || 1
    @users = User.order(rank: :desc).page(page).per(params[:per] || 10)

    response.headers['page'] = page.to_s
    response.headers['total'] = @users.all.count.to_s

    if @users
      render json: @users, status: :ok
    else
      render json: {message: 'no users'}, status: :not_found
    end
  end

  # web端获取总排名
  def get_total_rank_for_angular
    if request.headers['Range'].present?
      @start, @_end = request.headers['Range'].split('-')
    else
      @start, @_end = 0, 9
    end

    count=@_end.to_i - @start.to_i

    @users = User.order(rank: :desc)
    @result = @users.offset(@start).limit(count)

    total_count = @users.all.count.to_s
    response.headers['Accept-Ranges']='items'
    response.headers['Content-Range']="#{@start.to_i}-#{(@start.to_i+@result.length)}/#{total_count}"
    response.headers['Range-Unit']='items'

    if @users
      render json: @result, status: :ok
    else
      render json: {message: 'no users'}, status: :not_found
    end
  end

  # 用户认证
  def user_approve
    user_id = params[:user_id]
    stu_num = params[:stu_num]
    real_name = params[:real_name]

    major_id = (stu_num[4] + stu_num[6]).to_i
    @user = User.find_by(id: user_id)
    if @user.update_attributes(stu_num: stu_num, real_name: real_name, major_id: major_id)
      render json: @user, status: :ok
    else
      render json: @user.errors, status: :bad_request
    end
  end

  # 邮箱认证
  def school_email_approve
    user_id = params[:user_id]
    school_email = params[:school_email]
    @user = User.find_by(id: user_id)
    if @user.update_attributes(school_image: school_email)
      render json: @user, status: :ok
    else
      render json: @user.errors, status: :bad_request
    end
  end

  # 管理员认证
  def manager_approve

  end

  # 搜索好友
  def search_friend
    @user = User.find_by(params[:stu_num])
    if @user
      render json: @user, status: :ok
    else
      render json: {message: '学号不存在'}, status: :not_found
    end
  end

  # user对象需要参数
  def user_params
    params.require(:user).permit(:email, :password, :username, :real_name, :pinyin, :stu_num, :user_sign, :sex, :brith, :hobby,
                                 :phone, :remark, :head_image, :school_image, :fellow_id, :clazz, :default_org, :major_id,)
    # user_roles_attributes: [:role_id]
  end
end
