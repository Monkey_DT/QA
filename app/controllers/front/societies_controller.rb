class Front::SocietiesController < Front::BaseController
  def index
    @society = Society.all.page(params[:page]).per(params[:per])
    render json: @society
  end

  def show
    @society = Society.find_by(id:params[:id])
    if @society
      render json: @society
    else
      render json: {message:'id not exsit'}
    end
  end

  def update
    s_params = society_params
    @society = User.all.find_by(id:params[:id])
    if @society
      if @society.update_attributes(s_params)
        render json: @society , status: :created
      else
        render json: @society.errors , status: :bad_request
      end
    else
      render json: {message: 'id is not exist'} , status: :not_found
    end

  end

  def society_params
    params.require(:society).permit(:cover, :name, :master_id, :telephone, :introduction, :link)
  end
end
