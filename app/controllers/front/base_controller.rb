class Front::BaseController < ActionController::Base
  include BreadcurmbModule
  layout :front_layout
  before_action :user_form_token
  def front_layou
    request.headers['X-PJAX'] ? 'front/pjax' : 'front/application'
  end

  def user_form_token
    @current_user = User.find_by_authentication_token(params[:access_token] ||headers['Access-Token'])
    sign_in @current_user if @current_user
  end
end