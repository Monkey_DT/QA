class Front::NoticesController < Front::BaseController
  def index
    page = params[:page] || 1
    @notices = Notice.all.page(page).per(params[:per] || 10)

    response.headers['page'] = page.to_s
    response.headers['total'] = @notices.all.count.to_s

    if @notices
      render json: @notices , status: :ok
    else
      render json: @notices.errors , status: :bad_request
    end
  end

  def create
    @notice = Notice.new(notice_params)
    if @notice.save
      render json: @notice , status: :created
    else
      render json: @notice.errors , status: :bad_request
    end

  end

  def notice_params
    params.require(:notice).permit(:content, :title, :type, :extra_id, :extra_type)
  end
end
