class Front::AnswersController < Front::BaseController
  # 显示所有回答
  def index
    question_id = params[:question_id]
    page = params[:page] || 1
    @answers = Answer.all.where('question_id=?', question_id).order(created_at: :desc).page(page).per(params[:per] || 10)

    items=[]
    @answers.each do |item|
      items << item.answer_to_all_json
    end

    response.headers['page'] = page.to_s
    response.headers['total'] = Answer.all.count.to_s

    render json: items, status: :created
  end

  # web端显示所有回答
  def index_for_angular
    question_id = params[:question_id]
    if request.headers['Range'].present?
      @start, @_end = request.headers['Range'].split('-')
    else
      @start, @_end = 0, 9
    end

    count=@_end.to_i - @start.to_i

    @answers = Answer.all.where('question_id=?', question_id).order(created_at: :desc)
    @result = @answers.order(created_at: :desc).offset(@start).limit(count)

    items=[]
    @result.each do |item|
      items << item.answer_to_all_json
    end

    total_count = @answers.count.to_s
    response.headers['Accept-Ranges']='items'
    response.headers['Content-Range']="#{@start.to_i}-#{(@start.to_i+@result.length)}/#{total_count}"
    response.headers['Range-Unit']='items'


    render json: items, status: :created
  end

  def show
    @answer = Answer.find_by(id: params[:id])
    if @answer
      render json: @answer.answer_to_all_json
    else
      render json: {message: 'id not exsit'}
    end
  end

  # 点赞
  def praise
    user_id = params[:user_id]
    answer_id = params[:answer_id]

    @association = Association.where('A_id=? AND A_type=? AND B_id=? AND B_type=?',
                                     user_id, 'users', answer_id, 'answers').first
    if @association
      if @association.extra_message = '1'
        render json: {message: 'already praise'}
      end
    else
      if @answer = Answer.find_by(id:answer_id)
        @answer.praise_num += 1
        if @answer.update_attributes(praise_num:@answer.praise_num)
          @association = Association.new(:A_id => user_id,:A_type => 'users',
                                         :B_id => answer_id,:B_type => 'answers',
                                         :AB_association => 'UA')
          @association.save
          render json: {message: 'success'}, status: :ok
        else
          render json: {message: 'error'}
        end
      else
        render json: {message: 'id not exsit'}, status: :not_found
      end
    end
  end

  def answers_praise_num
    answer_id = params[:answer_id]
    @answer = Answer.find_by(id:answer_id)
    if @answer
      render json: @answer.praise_num , status: :ok
    else
      render json: {message:'id not exsit'} , status: :not_found
    end
  end

  # def answer_hottest
  #   page = params[:page] || 1
  #   @answers = answer.all.order(read_num: :desc).page(page).per(params[:per] || 10)
  #
  #   items=[]
  #   @answers.each do |item|
  #     items << item.answer_to_all_json
  #   end
  #
  #   response.headers['page'] = page.to_s
  #   response.headers['total'] = answer.all.count.to_s
  #
  #   render json: items , status: :created
  # end

  # 创建问题
  def create
    @answer = Answer.new(answer_params)
    if @answer.save
      @association = Association.new A_id:@answer.user_id , A_type: 'users' ,
                                     B_id:@answer.id, B_type:'answers',
                                     AB_association:'UA'
      @association.save
      render json: @answer, status: :created
    else
      render json: @answer.errors, status: :bad_request
    end
  end

  # 获取用户回答过的问题及答案
  def user_already_answer
    page = params[:page] || 1
    @answers = Answer.where(user_id: params[:user_id]).page(page).per(params[:per] || 10)

    items=[]
    @answers.each do |item|
      items << item.already_answer_to_all_json
    end

    response.headers['page'] = page.to_s
    response.headers['total'] = @answers.all.count.to_s

    if @answers
      render json: items , status: :ok
    else
      render json: {message:'id is not exsit'} , status: :not_found
    end
  end

  # web端获取用户回答过的问题及答案
  def user_already_answer_for_angular
    if request.headers['Range'].present?
      @start, @_end = request.headers['Range'].split('-')
    else
      @start, @_end = 0, 9
    end

    count=@_end.to_i - @start.to_i

    @answers = Answer.where(user_id: params[:user_id])
    @result = @answers.order(created_at: :desc).offset(@start).limit(count)

    items=[]
    @result.each do |item|
      items << item.already_answer_to_all_json
    end

    total_count = @answers.count.to_s
    response.headers['Accept-Ranges']='items'
    response.headers['Content-Range']="#{@start.to_i}-#{(@start.to_i+@result.length)}/#{total_count}"
    response.headers['Range-Unit']='items'


    if @answers
      render json: items , status: :ok
    else
      render json: {message:'id is not exsit'} , status: :not_found
    end
  end

  # 图片上传
  def create_with_picture
    @answer = Answer.new(answer_params_with_picture)
    if @answer.save
      @association.new A_id:@answer.user_id , A_type: 'users' , B_id:@answer.id, B_type:'answers',AB_association:'UA'
      if @association.save
        render json: @answer, status: :created
      else
        render json: @association.errors, status: :bad_request
      end
    else
      render json: @answer.errors, status: :bad_request
    end
  end

  def answer_params
    params.permit(:content, :praise_num, :user_id, :user_role, :question_id)
  end

  def answer_params_with_picture
    params.permit(:content, :praise_num, :user_id, :user_role, :question_id, :avator)
  end
end
