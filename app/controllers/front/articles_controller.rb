class Front::ArticlesController < Front::BaseController
  #创建问题
  def create
    @article_data = params[:articles_datas]
    @article_data.each do |v|
      p v.as_json
      @article = Article.new title: v[:title], content: v[:content],article_type_id: v[:article_type_id],author: v[:author],
                             link: v[:link],root: v[:root],intro: v[:intro]
      if @article.save
        @notice = Notice.new title: v[:title], content: v[:content],type: v[:article_type_id]
        render json: @article, status: :created
      else
        render json: @article.errors, status: :bad_request
      end
    end
  end

  def is_exsit
    url = params[:url]
    @article.find_by(link:url)
    if @article
      render json: {message:'true'}
    else
      render json: {message:'false'}
    end
  end

  def article_params(parameters)
    parameters ||= params
    parameters.permit(:content, :article_type_id, :author, :link, :root, :intro,:title)
  end
end
