class Front::MajorController < Front::BaseController
  def index
    @major = Major.all.page(params[:page]).per(params[:per])
    render json :@major
  end
end
