class Front::QuestionsController < Front::BaseController
  # 获取所有问题（最新）
  def index
    page = params[:page] || 1
    per = params[:per] || 10
    @questions = Question.all.order(created_at: :desc).page(page).per(per)

    items=[]
    @questions.each do |item|
      items << item.question_to_all_json
    end

    total_count = Question.all.count.to_s

    response.headers['page'] = page.to_s
    response.headers['total'] = total_count
    response.headers['Accept-Ranges']='items'
    response.headers['Content-Range']="#{(page.to_i-1)*per.to_i}-#{((page.to_i)*per.to_i)-1}/#{total_count}"
    response.headers['Range-Unit']='items'


    render json: items, status: :created
  end

  def index_for_angular
    if request.headers['Range'].present?
      @start, @_end = request.headers['Range'].split('-')
    else
      @start, @_end = 0, 9
    end

    title = params[:title]
    count=@_end.to_i - @start.to_i

    @questions = Question.all
    @questions = @questions.where("title LIKE ?", "%#{title}%") if title.present?

    @result = @questions.order(created_at: :desc).offset(@start).limit(count)

    items=[]
    @result.each do |item|
      items << item.question_to_all_json
    end

    total_count = @questions.count.to_s
    response.headers['Accept-Ranges']='items'
    response.headers['Content-Range']="#{@start.to_i}-#{(@start.to_i+@result.length)}/#{total_count}"
    response.headers['Range-Unit']='items'


    render json: items, status: :created
  end

  # 增加阅读量
  def add_read_num
    @question = Question.find_by(id:params[:question_id])
    @question.read_num += 1
    if @question.update_attributes(read_num:@question.read_num)
      render json: @question , status: :ok
    else
      render json: {message:'id not exsit'} , status: :not_found
    end
  end

  # 获取单个问题信息
  def show
    @question = Question.find_by(id: params[:id])
    if @question
      render json: @question.question_to_all_json
    else
      render json: {message: 'id not exsit'}
    end
  end

  # web端最热问题
  def question_hottest_for_angular
    if request.headers['Range'].present?
      @start, @_end = request.headers['Range'].split('-')
    else
      @start, @_end = 0, 9
    end

    count=@_end.to_i - @start.to_i

    @questions = Question.all.order(read_num: :desc)
    @result = @questions.offset(@start).limit(count)

    items=[]
    @result.each do |item|
      items << item.question_to_all_json
    end

    total_count = @questions.count.to_s
    response.headers['Accept-Ranges']='items'
    response.headers['Content-Range']="#{@start.to_i}-#{(@start.to_i+@result.length)}/#{total_count}"
    response.headers['Range-Unit']='items'


    render json: items, status: :created
  end

  # 获取所有最热问题
  def question_hottest
    page = params[:page] || 1
    @questions = Question.all.order(read_num: :desc).page(page).per(params[:per] || 10)

    items=[]
    @questions.each do |item|
      items << item.question_to_all_json
    end

    response.headers['page'] = page.to_s
    response.headers['total'] = Question.all.count.to_s

    render json: items, status: :created
  end

  # 创建问题
  def create
    @question = Question.new(question_params)
    if @question.save
      render json: @question, status: :created
    else
      render json: @question.errors, status: :bad_request
    end
  end

  # 搜索问题
  def search
    page = params[:page] || 1
    title = params[:title]

    @questions = Question.where("title LIKE ?", "%#{title}%").order(read_num: :desc).page(page).per(params[:per] || 10)

    response.headers['page'] = page.to_s
    response.headers['total'] = Question.where("title LIKE ?", "%#{title}%").all.count.to_s

    render json: @questions, status: :ok
  end

  # 获取用户推荐信息
  def recommend_info
    @user = User.find_by(params[:user_id])
    type = params[:type]
    if @user
      type == 0 ? recommend_info_major(@user.id) : recommend_info_fellow(@user.id)
    else
      render json: {message: 'id not exsit'}, status: :not_found
    end
  end

  # web端获取用户推荐信息
  def recommend_info_for_angular
    @user = User.find_by(params[:user_id])
    type = params[:type]
    if @user
      type == 0 ? recommend_info_major_for_angular(@user.id) : recommend_info_fellow_for_angular(@user.id)
    else
      render json: {message: 'id not exsit'}, status: :not_found
    end
  end

  # 老乡会推荐信息
  def recommend_info_fellow(userID)
    page = params[:page] || 1
    per = params[:per] || 10
    @fellow = Fellow.find_by(id: User.find_by(id: userID).fellow_id)

    response.headers['page'] = page.to_s
    response.headers['total'] =  @fellow.questions.length.to_s

    if @fellow
      render json: @fellow.questions.page(page).per(per), status: :ok
    else
      render json: {message: 'error'}, status: :not_found
    end
  end

  # web端老乡会推荐信息
  def recommend_info_fellow_for_angular(userID)
    if request.headers['Range'].present?
      @start, @_end = request.headers['Range'].split('-')
    else
      @start, @_end = 0, 9
    end

    count=@_end.to_i - @start.to_i

    @fellow = Fellow.find_by(id: User.find_by(id: userID).fellow_id)
    if @fellow
      @questions = @fellow.questions
      @result = @questions.order(created_at: :desc).offset(@start).limit(count)

      total_count = @questions.length
      response.headers['Accept-Ranges']='items'
      response.headers['Content-Range']="#{@start.to_i}-#{(@start.to_i+@result.length)}/#{total_count}"
      response.headers['Range-Unit']='items'


      render json: @result, status: :created
    else
      render json: {message:'fellow not exsit'}
    end

  end

  # 专业推荐信息
  def recommend_info_major(userID)
    page = params[:page] || 1
    per = params[:per] || 10

    @users = User.find_by(major_id: User.find_by(userID).major_id)

    response.headers['page'] = page.to_s
    response.headers['total'] =  @users.questions.length.to_s

    if @users
      render json: @users.questions.page(page).per(per), status: :ok
    else
      render json: {message: 'error'}, status: :not_found

    end
  end

  # web端专业推荐信息
  def recommend_info_major_for_angular(userID)
    if request.headers['Range'].present?
      @start, @_end = request.headers['Range'].split('-')
    else
      @start, @_end = 0, 9
    end

    count=@_end.to_i - @start.to_i

    @users = User.find_by(major_id: User.find_by(userID).major_id)
    @result = @users.order(created_at: :desc).offset(@start).limit(count)

    total_count = @users.questions.length
    response.headers['Accept-Ranges']='items'
    response.headers['Content-Range']="#{@start.to_i}-#{(@start.to_i+@result.length)}/#{total_count}"
    response.headers['Range-Unit']='items'


    render json: @result, status: :created
  end

  # 获取用户提出过的问题
  def user_already_ask
    page = params[:page] || 1
    @questions = Question.where(user_id: params[:user_id]).page(page).per(params[:per] || 10)

    response.headers['page'] = page.to_s
    response.headers['total'] = @questions.all.count.to_s

    items = []
    @questions.each do |item|
      items << item.question_to_all_json
    end

    if @questions
      render json: items, status: :ok
    else
      render json: {message: 'no questions'}, status: :not_found
    end
  end

  # web端获取用户提出过的问题
  def user_already_ask_for_angular
    if request.headers['Range'].present?
      @start, @_end = request.headers['Range'].split('-')
    else
      @start, @_end = 0, 9
    end

    count=@_end.to_i - @start.to_i

    @questions = Question.where(user_id: params[:user_id])
    @result = @questions.order(created_at: :desc).offset(@start).limit(count)

    total_count = @questions.length
    response.headers['Accept-Ranges']='items'
    response.headers['Content-Range']="#{@start.to_i}-#{(@start.to_i+@result.length)}/#{total_count}"
    response.headers['Range-Unit']='items'


    render json: @result, status: :created
  end

  # 图片上传创建问题
  def create_with_picture
    @question = Question.new(question_params_with_pricture)
    if @question.save
      render json: @question, status: :created
    else
      render json: @question.errors, status: :bad_request
    end
  end

  # 问题参数
  def question_params
    params.permit(:user_id, :content, :title, :label)
  end

  # 图片上传问题参数
  def question_params_with_pricture
    params.permit(:user_id, :content, :title, :label, :avator)
  end
end
