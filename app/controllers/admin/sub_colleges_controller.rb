# coding: UTF-8
class Admin::SubCollegesController < Admin::BaseController
  before_action :base_breadcrumb
  before_action :set_sub_college, only: [:show, :edit, :update, :destroy]
  before_action :name_breadcrumb_handler, only: [:show, :edit]
  before_action :action_common_breadcurmb, only: [:new, :edit]
  # GET /admin/sub_colleges
  def index
    @sub_colleges = initialize_grid(SubCollege, page: params[:page], order: :created_at, order_direction: :desc)
  end

  # GET /admin/sub_colleges/1
  def show
  end

  # GET /admin/sub_colleges/new
  def new
    @sub_college = SubCollege.new
  end

  # GET /admin/sub_colleges/1/edit
  def edit
  end

  # POST /admin/sub_colleges
  def create
    @sub_college = SubCollege.new(sub_college_params)

    respond_to do |format|
      if @sub_college.save
        format.html { redirect_to admin_sub_college_path(@sub_college.id), notice: create_success_notice(:sub_college) }
       format.json { render action: 'show', status: :created, location: @sub_college }
      else
        format.html { render action: 'new' }
        format.json { render json: @sub_college.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/sub_colleges/1
  def update
    respond_to do |format|
      if @sub_college.update_attributes(sub_college_params)
       format.html { redirect_to admin_sub_college_path(@sub_college.id), notice: update_success_notice(:sub_college) }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @sub_college.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/sub_colleges/1
  def destroy
    begin
      @sub_college.destroy
    rescue
      @sub_college.update_attributes(enable: false)
    end

    respond_to do |format|
      format.html { redirect_to admin_sub_colleges_path, notice: destroy_success_notice(:sub_college) }
      format.json { head :no_content }
    end
  end

  def destroy_multi
    ids =params[:grid][:select_ids]
    if ids and ids.instance_of? Array
      ids.each do |id|
        sub_college = SubCollege.find(id)
        begin
          sub_college.destroy! if sub_college
        rescue ActiveRecord::RecordNotDestroyed
          sub_college.update_attributes(enable: false)
          next
        end
      end
    end
  rescue
    flash[:notice] ="#{t 'common.delete_multi'}#{t 'common.fault'}"
    redirect_to action: 'index'
  else
    flash[:notice] ="#{t 'common.delete_multi'}#{t 'common.success'}"
    redirect_to action: 'index'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_sub_college
    if params[:action] == 'show'
       @sub_college = SubCollege.find(params[:id])
    else
      @sub_college = SubCollege.find(params[:id])
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def sub_college_params
    params.require(:sub_college).permit(:name)
  end

  def base_breadcrumb
    add_breadcrumb (t 'activerecord.attributes.sub_college.admin_title'), admin_sub_colleges_path
  end

  # add name breadcrumb with :show, :edit
  def name_breadcrumb_handler
    add_breadcrumb @sub_college.id, admin_sub_college_path(@sub_college.id), class: :pjax
  end
end