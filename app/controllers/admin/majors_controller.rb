# coding: UTF-8
class Admin::MajorsController < Admin::BaseController
  before_action :base_breadcrumb
  before_action :set_major, only: [:show, :edit, :update, :destroy]
  before_action :name_breadcrumb_handler, only: [:show, :edit]
  before_action :action_common_breadcurmb, only: [:new, :edit]
  # GET /admin/majors
  def index
    @majors = initialize_grid(Major, page: params[:page], order: :created_at, order_direction: :desc)
  end

  # GET /admin/majors/1
  def show
  end

  # GET /admin/majors/new
  def new
    @major = Major.new
  end

  # GET /admin/majors/1/edit
  def edit
  end

  # POST /admin/majors
  def create
    @major = Major.new(major_params)

    respond_to do |format|
      if @major.save
        format.html { redirect_to admin_major_path(@major.id), notice: create_success_notice(:major) }
       format.json { render action: 'show', status: :created, location: @major }
      else
        format.html { render action: 'new' }
        format.json { render json: @major.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/majors/1
  def update
    respond_to do |format|
      if @major.update_attributes(major_params)
       format.html { redirect_to admin_major_path(@major.id), notice: update_success_notice(:major) }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @major.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/majors/1
  def destroy
    begin
      @major.destroy
    rescue
      @major.update_attributes(enable: false)
    end

    respond_to do |format|
      format.html { redirect_to admin_majors_path, notice: destroy_success_notice(:major) }
      format.json { head :no_content }
    end
  end

  def destroy_multi
    ids =params[:grid][:select_ids]
    if ids and ids.instance_of? Array
      ids.each do |id|
        major = Major.find(id)
        begin
          major.destroy! if major
        rescue ActiveRecord::RecordNotDestroyed
          major.update_attributes(enable: false)
          next
        end
      end
    end
  rescue
    flash[:notice] ="#{t 'common.delete_multi'}#{t 'common.fault'}"
    redirect_to action: 'index'
  else
    flash[:notice] ="#{t 'common.delete_multi'}#{t 'common.success'}"
    redirect_to action: 'index'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_major
    if params[:action] == 'show'
       @major = Major.find(params[:id])
    else
      @major = Major.find(params[:id])
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def major_params
    params.require(:major).permit(:no, :name, :sub_college_id)
  end

  def base_breadcrumb
    add_breadcrumb (t 'activerecord.attributes.major.admin_title'), admin_majors_path
  end

  # add name breadcrumb with :show, :edit
  def name_breadcrumb_handler
    add_breadcrumb @major.id, admin_major_path(@major.id), class: :pjax
  end
end