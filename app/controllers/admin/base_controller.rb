class Admin::BaseController < ApplicationController
  include BreadcurmbModule

  before_action :authenticate_user!#, unless: [:other_controller]

  layout :admin_layout
  def admin_layout
    request.headers['X-PJAX'] ? 'admin/pjax' : 'admin/application'
  end

end