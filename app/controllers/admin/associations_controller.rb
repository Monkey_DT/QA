# coding: UTF-8
class Admin::AssociationsController < Admin::BaseController
  before_action :base_breadcrumb
  before_action :set_association, only: [:show, :edit, :update, :destroy]
  before_action :name_breadcrumb_handler, only: [:show, :edit]
  before_action :action_common_breadcurmb, only: [:new, :edit]
  # GET /admin/associations
  def index
    @associations = initialize_grid(Association, page: params[:page], order: :created_at, order_direction: :desc)
  end

  # GET /admin/associations/1
  def show
  end

  # GET /admin/associations/new
  def new
    @association = Association.new
  end

  # GET /admin/associations/1/edit
  def edit
  end

  # POST /admin/associations
  def create
    @association = Association.new(association_params)

    respond_to do |format|
      if @association.save
        format.html { redirect_to admin_association_path(@association.id), notice: create_success_notice(:association) }
       format.json { render action: 'show', status: :created, location: @association }
      else
        format.html { render action: 'new' }
        format.json { render json: @association.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/associations/1
  def update
    respond_to do |format|
      if @association.update_attributes(association_params)
       format.html { redirect_to admin_association_path(@association.id), notice: update_success_notice(:association) }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @association.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/associations/1
  def destroy
    begin
      @association.destroy
    rescue
      @association.update_attributes(enable: false)
    end

    respond_to do |format|
      format.html { redirect_to admin_associations_path, notice: destroy_success_notice(:association) }
      format.json { head :no_content }
    end
  end

  def destroy_multi
    ids =params[:grid][:select_ids]
    if ids and ids.instance_of? Array
      ids.each do |id|
        association = Association.find(id)
        begin
          association.destroy! if association
        rescue ActiveRecord::RecordNotDestroyed
          association.update_attributes(enable: false)
          next
        end
      end
    end
  rescue
    flash[:notice] ="#{t 'common.delete_multi'}#{t 'common.fault'}"
    redirect_to action: 'index'
  else
    flash[:notice] ="#{t 'common.delete_multi'}#{t 'common.success'}"
    redirect_to action: 'index'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_association
    if params[:action] == 'show'
       @association = Association.find(params[:id])
    else
      @association = Association.find(params[:id])
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def association_params
    params.require(:association).permit(:A_id, :A_type, :B_id, :B_type, :AB_association)
  end

  def base_breadcrumb
    add_breadcrumb (t 'activerecord.attributes.association.admin_title'), admin_associations_path
  end

  # add name breadcrumb with :show, :edit
  def name_breadcrumb_handler
    add_breadcrumb @association.id, admin_association_path(@association.id), class: :pjax
  end
end