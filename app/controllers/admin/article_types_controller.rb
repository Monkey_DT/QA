# coding: UTF-8
class Admin::ArticleTypesController < Admin::BaseController
  before_action :base_breadcrumb
  before_action :set_article_type, only: [:show, :edit, :update, :destroy]
  before_action :name_breadcrumb_handler, only: [:show, :edit]
  before_action :action_common_breadcurmb, only: [:new, :edit]
  # GET /admin/article_types
  def index
    @article_types = initialize_grid(ArticleType, page: params[:page], order: :created_at, order_direction: :desc)
  end

  # GET /admin/article_types/1
  def show
  end

  # GET /admin/article_types/new
  def new
    @article_type = ArticleType.new
  end

  # GET /admin/article_types/1/edit
  def edit
  end

  # POST /admin/article_types
  def create
    @article_type = ArticleType.new(article_type_params)

    respond_to do |format|
      if @article_type.save
        format.html { redirect_to admin_article_type_path(@article_type.id), notice: create_success_notice(:article_type) }
       format.json { render action: 'show', status: :created, location: @article_type }
      else
        format.html { render action: 'new' }
        format.json { render json: @article_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/article_types/1
  def update
    respond_to do |format|
      if @article_type.update_attributes(article_type_params)
       format.html { redirect_to admin_article_type_path(@article_type.id), notice: update_success_notice(:article_type) }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @article_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/article_types/1
  def destroy
    begin
      @article_type.destroy
    rescue
      @article_type.update_attributes(enable: false)
    end

    respond_to do |format|
      format.html { redirect_to admin_article_types_path, notice: destroy_success_notice(:article_type) }
      format.json { head :no_content }
    end
  end

  def destroy_multi
    ids =params[:grid][:select_ids]
    if ids and ids.instance_of? Array
      ids.each do |id|
        article_type = ArticleType.find(id)
        begin
          article_type.destroy! if article_type
        rescue ActiveRecord::RecordNotDestroyed
          article_type.update_attributes(enable: false)
          next
        end
      end
    end
  rescue
    flash[:notice] ="#{t 'common.delete_multi'}#{t 'common.fault'}"
    redirect_to action: 'index'
  else
    flash[:notice] ="#{t 'common.delete_multi'}#{t 'common.success'}"
    redirect_to action: 'index'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_article_type
    if params[:action] == 'show'
       @article_type = ArticleType.find(params[:id])
    else
      @article_type = ArticleType.find(params[:id])
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def article_type_params
    params.require(:article_type).permit(:name, :prosition)
  end

  def base_breadcrumb
    add_breadcrumb (t 'activerecord.attributes.article_type.admin_title'), admin_article_types_path
  end

  # add name breadcrumb with :show, :edit
  def name_breadcrumb_handler
    add_breadcrumb @article_type.id, admin_article_type_path(@article_type.id), class: :pjax
  end
end