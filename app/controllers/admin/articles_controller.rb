# coding: UTF-8
class Admin::ArticlesController < Admin::BaseController
  before_action :base_breadcrumb
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  before_action :name_breadcrumb_handler, only: [:show, :edit]
  before_action :action_common_breadcurmb, only: [:new, :edit]
  # GET /admin/articles
  def index
    @articles = initialize_grid(Article, page: params[:page], order: :created_at, order_direction: :desc)
  end

  # GET /admin/articles/1
  def show
  end

  # GET /admin/articles/new
  def new
    @article = Article.new
  end

  # GET /admin/articles/1/edit
  def edit
  end

  # POST /admin/articles
  def create
    @article = Article.new(article_params)

    respond_to do |format|
      if @article.save
        format.html { redirect_to admin_article_path(@article.id), notice: create_success_notice(:article) }
       format.json { render action: 'show', status: :created, location: @article }
      else
        format.html { render action: 'new' }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/articles/1
  def update
    respond_to do |format|
      if @article.update_attributes(article_params)
       format.html { redirect_to admin_article_path(@article.id), notice: update_success_notice(:article) }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/articles/1
  def destroy
    begin
      @article.destroy
    rescue
      @article.update_attributes(enable: false)
    end

    respond_to do |format|
      format.html { redirect_to admin_articles_path, notice: destroy_success_notice(:article) }
      format.json { head :no_content }
    end
  end

  def destroy_multi
    ids =params[:grid][:select_ids]
    if ids and ids.instance_of? Array
      ids.each do |id|
        article = Article.find(id)
        begin
          article.destroy! if article
        rescue ActiveRecord::RecordNotDestroyed
          article.update_attributes(enable: false)
          next
        end
      end
    end
  rescue
    flash[:notice] ="#{t 'common.delete_multi'}#{t 'common.fault'}"
    redirect_to action: 'index'
  else
    flash[:notice] ="#{t 'common.delete_multi'}#{t 'common.success'}"
    redirect_to action: 'index'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_article
    if params[:action] == 'show'
       @article = Article.find(params[:id])
    else
      @article = Article.find(params[:id])
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def article_params
    params.require(:article).permit(:content, :article_type_id, :author, :link, :source_id)
  end

  def base_breadcrumb
    add_breadcrumb (t 'activerecord.attributes.article.admin_title'), admin_articles_path
  end

  # add name breadcrumb with :show, :edit
  def name_breadcrumb_handler
    add_breadcrumb @article.id, admin_article_path(@article.id), class: :pjax
  end
end