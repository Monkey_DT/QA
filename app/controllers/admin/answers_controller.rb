# coding: UTF-8
class Admin::AnswersController < Admin::BaseController
  before_action :base_breadcrumb
  before_action :set_answer, only: [:show, :edit, :update, :destroy]
  before_action :name_breadcrumb_handler, only: [:show, :edit]
  before_action :action_common_breadcurmb, only: [:new, :edit]
  # GET /admin/answers
  def index
    @answers = initialize_grid(Answer, page: params[:page], order: :created_at, order_direction: :desc)
  end

  # GET /admin/answers/1
  def show
  end

  # GET /admin/answers/new
  def new
    @answer = Answer.new
  end

  # GET /admin/answers/1/edit
  def edit
  end

  # POST /admin/answers
  def create
    @answer = Answer.new(answer_params)

    respond_to do |format|
      if @answer.save
        format.html { redirect_to admin_answer_path(@answer.id), notice: create_success_notice(:answer) }
       format.json { render action: 'show', status: :created, location: @answer }
      else
        format.html { render action: 'new' }
        format.json { render json: @answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/answers/1
  def update
    respond_to do |format|
      if @answer.update_attributes(answer_params)
       format.html { redirect_to admin_answer_path(@answer.id), notice: update_success_notice(:answer) }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/answers/1
  def destroy
    begin
      @answer.destroy
    rescue
      @answer.update_attributes(enable: false)
    end

    respond_to do |format|
      format.html { redirect_to admin_answers_path, notice: destroy_success_notice(:answer) }
      format.json { head :no_content }
    end
  end

  def destroy_multi
    ids =params[:grid][:select_ids]
    if ids and ids.instance_of? Array
      ids.each do |id|
        answer = Answer.find(id)
        begin
          answer.destroy! if answer
        rescue ActiveRecord::RecordNotDestroyed
          answer.update_attributes(enable: false)
          next
        end
      end
    end
  rescue
    flash[:notice] ="#{t 'common.delete_multi'}#{t 'common.fault'}"
    redirect_to action: 'index'
  else
    flash[:notice] ="#{t 'common.delete_multi'}#{t 'common.success'}"
    redirect_to action: 'index'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_answer
    if params[:action] == 'show'
       @answer = Answer.find(params[:id])
    else
      @answer = Answer.find(params[:id])
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def answer_params
    params.require(:answer).permit(:content, :praise_num, :user_id, :user_role, :question_id)
  end

  def base_breadcrumb
    add_breadcrumb (t 'activerecord.attributes.answer.admin_title'), admin_answers_path
  end

  # add name breadcrumb with :show, :edit
  def name_breadcrumb_handler
    add_breadcrumb @answer.id, admin_answer_path(@answer.id), class: :pjax
  end
end