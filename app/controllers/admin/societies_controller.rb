# coding: UTF-8
class Admin::SocietiesController < Admin::BaseController
  before_action :base_breadcrumb
  before_action :set_society, only: [:show, :edit, :update, :destroy]
  before_action :name_breadcrumb_handler, only: [:show, :edit]
  before_action :action_common_breadcurmb, only: [:new, :edit]
  # GET /admin/societies
  def index
    @societies = initialize_grid(Society, page: params[:page], order: :created_at, order_direction: :desc)
  end

  # GET /admin/societies/1
  def show
  end

  # GET /admin/societies/new
  def new
    @society = Society.new
  end

  # GET /admin/societies/1/edit
  def edit
  end

  # POST /admin/societies
  def create
    @society = Society.new(society_params)

    respond_to do |format|
      if @society.save
        format.html { redirect_to admin_society_path(@society.id), notice: create_success_notice(:society) }
       format.json { render action: 'show', status: :created, location: @society }
      else
        format.html { render action: 'new' }
        format.json { render json: @society.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/societies/1
  def update
    respond_to do |format|
      if @society.update_attributes(society_params)
       format.html { redirect_to admin_society_path(@society.id), notice: update_success_notice(:society) }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @society.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/societies/1
  def destroy
    begin
      @society.destroy
    rescue
      @society.update_attributes(enable: false)
    end

    respond_to do |format|
      format.html { redirect_to admin_societies_path, notice: destroy_success_notice(:society) }
      format.json { head :no_content }
    end
  end

  def destroy_multi
    ids =params[:grid][:select_ids]
    if ids and ids.instance_of? Array
      ids.each do |id|
        society = Society.find(id)
        begin
          society.destroy! if society
        rescue ActiveRecord::RecordNotDestroyed
          society.update_attributes(enable: false)
          next
        end
      end
    end
  rescue
    flash[:notice] ="#{t 'common.delete_multi'}#{t 'common.fault'}"
    redirect_to action: 'index'
  else
    flash[:notice] ="#{t 'common.delete_multi'}#{t 'common.success'}"
    redirect_to action: 'index'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_society
    if params[:action] == 'show'
       @society = Society.find(params[:id])
    else
      @society = Society.find(params[:id])
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def society_params
    params.require(:society).permit(:cover, :name, :master_id, :telephone, :introduction, :link)
  end

  def base_breadcrumb
    add_breadcrumb (t 'activerecord.attributes.society.admin_title'), admin_societies_path
  end

  # add name breadcrumb with :show, :edit
  def name_breadcrumb_handler
    add_breadcrumb @society.id, admin_society_path(@society.id), class: :pjax
  end
end