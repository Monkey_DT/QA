# coding: UTF-8
class Admin::FellowsController < Admin::BaseController
  before_action :base_breadcrumb
  before_action :set_fellow, only: [:show, :edit, :update, :destroy]
  before_action :name_breadcrumb_handler, only: [:show, :edit]
  before_action :action_common_breadcurmb, only: [:new, :edit]
  # GET /admin/fellows
  def index
    @fellows = initialize_grid(Fellow, page: params[:page], order: :created_at, order_direction: :desc)
  end

  # GET /admin/fellows/1
  def show
  end

  # GET /admin/fellows/new
  def new
    @fellow = Fellow.new
  end

  # GET /admin/fellows/1/edit
  def edit
  end

  # POST /admin/fellows
  def create
    @fellow = Fellow.new(fellow_params)

    respond_to do |format|
      if @fellow.save
        format.html { redirect_to admin_fellow_path(@fellow.id), notice: create_success_notice(:fellow) }
       format.json { render action: 'show', status: :created, location: @fellow }
      else
        format.html { render action: 'new' }
        format.json { render json: @fellow.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/fellows/1
  def update
    respond_to do |format|
      if @fellow.update_attributes(fellow_params)
       format.html { redirect_to admin_fellow_path(@fellow.id), notice: update_success_notice(:fellow) }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @fellow.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/fellows/1
  def destroy
    begin
      @fellow.destroy
    rescue
      @fellow.update_attributes(enable: false)
    end

    respond_to do |format|
      format.html { redirect_to admin_fellows_path, notice: destroy_success_notice(:fellow) }
      format.json { head :no_content }
    end
  end

  def destroy_multi
    ids =params[:grid][:select_ids]
    if ids and ids.instance_of? Array
      ids.each do |id|
        fellow = Fellow.find(id)
        begin
          fellow.destroy! if fellow
        rescue ActiveRecord::RecordNotDestroyed
          fellow.update_attributes(enable: false)
          next
        end
      end
    end
  rescue
    flash[:notice] ="#{t 'common.delete_multi'}#{t 'common.fault'}"
    redirect_to action: 'index'
  else
    flash[:notice] ="#{t 'common.delete_multi'}#{t 'common.success'}"
    redirect_to action: 'index'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_fellow
    if params[:action] == 'show'
       @fellow = Fellow.find(params[:id])
    else
      @fellow = Fellow.find(params[:id])
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def fellow_params
    params.require(:fellow).permit(:name)
  end

  def base_breadcrumb
    add_breadcrumb (t 'activerecord.attributes.fellow.admin_title'), admin_fellows_path
  end

  # add name breadcrumb with :show, :edit
  def name_breadcrumb_handler
    add_breadcrumb @fellow.id, admin_fellow_path(@fellow.id), class: :pjax
  end
end