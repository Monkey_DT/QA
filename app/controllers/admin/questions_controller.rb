# coding: UTF-8
class Admin::QuestionsController < Admin::BaseController
  before_action :base_breadcrumb
  before_action :set_question, only: [:show, :edit, :update, :destroy]
  before_action :name_breadcrumb_handler, only: [:show, :edit]
  before_action :action_common_breadcurmb, only: [:new, :edit]
  # GET /admin/questions
  def index
    @questions = initialize_grid(Question, page: params[:page], order: :created_at, order_direction: :desc)
  end

  # GET /admin/questions/1
  def show
  end

  # GET /admin/questions/new
  def new
    @question = Question.new
  end

  # GET /admin/questions/1/edit
  def edit
  end

  # POST /admin/questions
  def create
    @question = Question.new(question_params)

    respond_to do |format|
      if @question.save
        format.html { redirect_to admin_question_path(@question.id), notice: create_success_notice(:question) }
       format.json { render action: 'show', status: :created, location: @question }
      else
        format.html { render action: 'new' }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/questions/1
  def update
    respond_to do |format|
      if @question.update_attributes(question_params)
       format.html { redirect_to admin_question_path(@question.id), notice: update_success_notice(:question) }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/questions/1
  def destroy
    begin
      @question.destroy
    rescue
      @question.update_attributes(enable: false)
    end

    respond_to do |format|
      format.html { redirect_to admin_questions_path, notice: destroy_success_notice(:question) }
      format.json { head :no_content }
    end
  end

  def destroy_multi
    ids =params[:grid][:select_ids]
    if ids and ids.instance_of? Array
      ids.each do |id|
        question = Question.find(id)
        begin
          question.destroy! if question
        rescue ActiveRecord::RecordNotDestroyed
          question.update_attributes(enable: false)
          next
        end
      end
    end
  rescue
    flash[:notice] ="#{t 'common.delete_multi'}#{t 'common.fault'}"
    redirect_to action: 'index'
  else
    flash[:notice] ="#{t 'common.delete_multi'}#{t 'common.success'}"
    redirect_to action: 'index'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_question
    if params[:action] == 'show'
       @question = Question.find(params[:id])
    else
      @question = Question.find(params[:id])
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def question_params
    params.require(:question).permit(:user_id, :content, :title, :label, :read_num)
  end

  def base_breadcrumb
    add_breadcrumb (t 'activerecord.attributes.question.admin_title'), admin_questions_path
  end

  # add name breadcrumb with :show, :edit
  def name_breadcrumb_handler
    add_breadcrumb @question.id, admin_question_path(@question.id), class: :pjax
  end
end