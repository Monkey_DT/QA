class Fellow < ActiveRecord::Base
  has_many :users
  has_many :questions, through: :users
  # has_many :associations, foreign_key: 'B_id'
end
