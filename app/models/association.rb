class Association < ActiveRecord::Base
  belongs_to :user, foreign_key: 'A_id'
  belongs_to :fellow, foreign_key: 'B_id'
end
