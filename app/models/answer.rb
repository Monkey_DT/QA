class Answer < ActiveRecord::Base
  belongs_to :question
  belongs_to :user

  # has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  # validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  def answer_to_all_json
    answer_json = self.as_json()
    answer_json[:head_image] = self.user_id.present? ? self.user.head_image : '' if self.user
    answer_json[:user_sign] = self.user_id.present? ? self.user.user_sign : '' if self.user
    return answer_json
  end

  def already_answer_to_all_json
    answer_json = self.as_json(only:[:content])
    answer_json[:questionId] = self.question_id.present? ? self.question.id : '' if self.question
    answer_json[:questionTitle] = self.question_id.present? ? self.question.title : '' if self.question
    answer_json[:questionCover] = self.user_id.present? ? self.user.head_image : '' if self.user
    answer_json[:questionName] = self.user_id.present? ? self.user.username : '' if self.user
    answer_json[:questionContent] = self.question_id.present? ? self.question.content : '' if self.question
    return answer_json
  end
end
