class Question < ActiveRecord::Base
  has_many :answers
  belongs_to :user
  # has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  # validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  def question_to_all_json
    question_json = self.as_json()
    question_json[:username] = self.user_id.present? ? self.user.username : '' if self.user
    question_json[:head_image] = self.user_id.present? ? self.user.head_image : '' if self.user
    return question_json
  end

end
