json.array!(@admin_answers) do |admin_answer|
  json.extract! admin_answer, :id, :content, :praise_num, :user_id, :user_role, :owner_id, :owner_type
  json.url admin_answer_url(admin_answer, format: :json)
end
