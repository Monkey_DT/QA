json.array!(@admin_notices) do |admin_notice|
  json.extract! admin_notice, :id, :content, :title, :type, :extra_id, :extra_type
  json.url admin_notice_url(admin_notice, format: :json)
end
