json.array!(@admin_societies) do |admin_society|
  json.extract! admin_society, :id, :cover, :name, :master_id, :telephone, :introduction, :link
  json.url admin_society_url(admin_society, format: :json)
end
