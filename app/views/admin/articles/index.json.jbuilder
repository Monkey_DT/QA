json.array!(@admin_articles) do |admin_article|
  json.extract! admin_article, :id, :content, :article_type_id, :author, :link, :source_id
  json.url admin_article_url(admin_article, format: :json)
end
