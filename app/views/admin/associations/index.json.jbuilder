json.array!(@admin_associations) do |admin_association|
  json.extract! admin_association, :id, :A_id, :A_type, :B_id, :B_type, :AB_association
  json.url admin_association_url(admin_association, format: :json)
end
