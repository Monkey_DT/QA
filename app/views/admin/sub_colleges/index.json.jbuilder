json.array!(@admin_sub_colleges) do |admin_sub_college|
  json.extract! admin_sub_college, :id, :name
  json.url admin_sub_college_url(admin_sub_college, format: :json)
end
