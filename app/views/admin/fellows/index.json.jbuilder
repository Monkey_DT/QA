json.array!(@admin_fellows) do |admin_fellow|
  json.extract! admin_fellow, :id, :name
  json.url admin_fellow_url(admin_fellow, format: :json)
end
