json.array!(@admin_majors) do |admin_major|
  json.extract! admin_major, :id, :no, :name, :sub_college_id
  json.url admin_major_url(admin_major, format: :json)
end
