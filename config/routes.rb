Scaffold::Application.routes.draw do
  devise_for :users, skip: [:registrations], controllers: {sessions: 'common/sessions'}
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'admin/users#index'
  get 'admin' => 'admin/users#index'
  namespace :front do |front|
    namespace :users do
      get :show
      post :sign_up
      post :validate, action: :validate!
      get :current
      post :sign_in_for_web
      post :sign_out_for_web
      get :get_rank
      post :user_approve
      get :search_friend
      get :school_email_approve
      get :get_total_rank
      get :get_total_rank_for_angular
      post :update
      post :update_only_four_info
    end
    resources :contacts do
      collection do
        get :upload
        post :upload_file
        get :tree_data, format: 'json'
      end
    end

    get 'users/edit' => 'users#edit'
    patch 'users/:id' => 'users#update'
    get 'users/update_password' => 'users#update_password'
    patch 'users/update_password' => 'users#update_password'
    resources :user_bills, only: [:index, :show]
    resources :answers do
      collection do
        post 'praise'
        get 'index_for_angular'
        get 'user_already_answer'
        get 'user_already_answer_for_angular'
        get 'answers_praise_num'
        post 'create_with_picture'
      end
    end
    # post 'answers/praise' => 'answers#praise'
    # get 'answers/user_already_answer' => 'answers#user_already_answer'
    resources :articles
    resources :article_types
    post 'association/judge_praise' => 'association#judge_praise'
    post 'association/fellow_list' => 'association#fellow_list'
    get 'association/fellow_list' => 'association#fellow_list'
    resources :fellows
    resources :majors
    resources :notices
    resources :questions do
      collection do
        get 'index_for_angular'
        post 'search'
        get 'recommend_info'
        get 'user_already_ask'
        get 'add_read_num'
        get 'question_hottest'
        get 'recommend_info_for_angular'
        get 'user_already_ask_for_angular'
        get 'question_hottest_for_angular'
        post 'create_with_picture'
      end
    end
    # post 'questions/search' => 'questions#search'
    # get 'questions/recommend_info' => 'questions#recommend_info'
    # get 'questions/user_already_ask' => 'questions#user_already_ask'
    resources :societies
    resources :sources
    resources :sub_colleges
  end

  namespace :admin do |admin|

    get 'roles/tree_data/:type' => 'roles#tree_data'
    post 'roles/destroy_multi' => 'roles#destroy_multi'
    post 'users/destroy_multi' => 'users#destroy_multi'
    post 'permissions/destroy_multi' => 'permissions#destroy_multi'
    post 'organizations/destroy_multi' => 'organizations#destroy_multi'
    post 'code_tables/destroy_multi' => 'code_tables#destroy_multi'
    post 'attachments/destroy_multi' => 'attachments#destroy_multi'
    post 'answers/destroy_multi' => 'answers#destroy_multi'
    post 'article_types/destroy_multi' => 'article_types#destroy_multi'
    post 'articles/destroy_multi' => 'articles#destroy_multi'
    post 'associations/destroy_multi' => 'associations#destroy_multi'
    post 'fellows/destroy_multi' => 'fellows#destroy_multi'
    post 'majors/destroy_multi' => 'majors#destroy_multi'
    post 'notices/destroy_multi' => 'notices#destroy_multi'
    post 'questions/destroy_multi' => 'questions#destroy_multi'
    post 'societies/destroy_multi' => 'societies#destroy_multi'
    post 'sources/destroy_multi' => 'sources#destroy_multi'
    post 'sub_colleges/destroy_multi' => 'sub_colleges#destroy_multi'
    resources :roles
    resources :attachments
    resources :code_tables
    resources :organizations
    resources :permissions
    resources :answers
    resources :articles
    resources :article_types
    resources :associations
    resources :fellows
    resources :majors
    resources :notices
    resources :questions
    resources :societies
    resources :sources
    resources :sub_colleges
    resources :users do
      collection do
        get 'validate_user'
        get 'update_password'
        patch 'update_password'
      end
    end
  end

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
